<?php

return [
    "home"=>"Главная",
    "about_us"=>"О нас",
    "services"=>"Наши услуги",
    "faqs"=>"Вопросы-ответы",
    "contacts"=>"Контакты",
    "sign_up"=>"Регистрация",
    "sign_in"=>"Войти"
];