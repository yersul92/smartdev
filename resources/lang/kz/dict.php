<?php

return [
    "home"=>"Басты бет",
    "about_us"=>"Біз туралы",
    "services"=>"Қызмет түрлері",
    "faqs"=>"Сұрақ-жауап",
    "contacts"=>"Байланыс",
    "sign_up"=>"Тіркелу",
    "sign_in"=>"Кіру"
];