<?php

return [
    "home"=>"Home",
    "about_us"=>"About us",
    "services"=>"Services",
    "faqs"=>"FAQs",
    "contacts"=>"Contacts",
    "sign_up"=>"Sign up",
    "sign_in"=>"Sign in"
];