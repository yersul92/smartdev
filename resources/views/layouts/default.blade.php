<!doctype html>
    <html lang="en" class=" desktop landscape">
        <head>
            @include('includes.head')
        </head>
        <body>
            <header id="header">
                @include('includes.header')
            </header>
            <div id="content">
                @yield('content')
            </div>
            <footer>
                @include('includes.footer')
            </footer>
        </body>
        </html>
