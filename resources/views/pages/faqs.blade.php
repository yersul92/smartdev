@extends('layouts.default')
@section('content')
    <div class="block-4 bg-1 indent-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<h2 class="title title-3">FAQs</h2>
						<ul class="list list-5">
							<li>
								<a href="#">Excepteur sint occaecat cupidatat</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Quisque consectetur diam id justo sollicitudin</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Duis tellus ligula, ullamcorper eu ultricies quis, varius sed erat</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Nulla ut massa at lacus scelerisque placerat</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Quisque rhoncus augue sit amet sem auctor venenatis</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Praesent iaculis rutrum sapien, vitae </a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">sapien, vitae pulvinar tortor vehicula vel</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Fusce porta, magna vel rutrum rhoncus, lorem lacus </a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">nulla nibh, volutpat et arcu eu, volutpat feugiat sem</a>
								<ul>
									<li>
										<h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,totam rem aperiam.</h6>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
											posuere cubilia Curae; Integer eget porttitor purus, sit amet aliquet nibh. Sed non efficitur mauris, vitae ultrices
											tellus. Praesent eu massa ac ante. Duis quis elit sed est consequat iaculis. Nam in egestas lorem. Donec dictum
											nulla non urna porta ultrices. Suspendisse non suscipit erouis sed velit magna. Nulla ut massa at lacus scelerisque
											placerat. </p>
										<p>Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper, maximus eros eget, congue lectus. Mauris non
											felis eu felis ultricies condimen venenatis quis nec ante. Integer sodales finibus venenatis. Duis sed velit magna.
											Nulla ut massa at lacus scelerisque placerat. Donec tempus dolor ut feugiat fringilla. Ut nec nisl ullamcorper,
											maximus eros eget, congue lectus. Mauris non felis eu felis ultricies condimentum id pellentesque ante. In elementum
											nulla eu rhoncus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
											Integer sodales finibus venenatis. Proin viverra lorem et massa blandit posuere. In vel elit ac turpis rutrum
											auctor eu ut orci. Curabitur vitae felis non enim interdum tristique eget vitae elit. Phasellus varius erat sed
											suscipit vehicula. Ut nec fringilla mi, eget consectetur mauris. </p>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<h2 class="title title-3">Search by subject</h2>
						<div class="box box-3">
							<ul class="list list-2">
								<li>Amet, consectetur </li>
								<li>Adipiscing elit fusce dapibus </li>
								<li>Nisi ex vel dictum dui pretium</li>
								<li>Praesent lacinia </li>
								<li>Dui eget risus ultricies</li>
								<li>Id venenatis enim ultrices</li>
								<li>Curabitur sit amet </li>
								<li>Vivamus eros augue</li>
								<li>Luctus pharetra mollis fringilla</li>
							</ul>
						</div>
						<h2 class="title title-3">List of questions</h2>
						<ul class="list list-6">
							<li>
								<a href="#">Sed ut perspiciatis unde omnis?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Elit varius, cursus ipsum sit amet?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Suada id nibh quis vulputate?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Mauris non felis eu felis ultricies?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Vestibulum tristique velit lectus?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">Proin viverra lorem et massa?</a>
								<ul>
									<li>
										<p>Quisque consectetur diam id justo sollicitudin, non mollis ligula commodo. Nulla ac lobortis quam. Fusce ac elit
											varius, cursus ipsum sit amet, auctor purus. In pretium scelerisque massa consectetur lacinia. Duis tellus ligula,
											ullamcorper eu ultricies quis, varius sed erat. Vestibulum ante ipsum.</p>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<script>
		function ddList(elementSel) {
			var element = jQuery(elementSel);
			element.find("li").each(function () {
				if (jQuery("ul", this).length > 0) {
					jQuery(this).addClass("with-ul");
					jQuery("ul", this).hide();
				}
			});
		}
		jQuery(document).ready(function () {
			ddList("ul.list-5, ul.list-6");
			jQuery("ul.list-5 li, ul.list-6 li").on("click", function () {
				jQuery(this).find("ul").slideToggle("fast");
				jQuery(this).toggleClass("active");
				return false;
			});
		});
	</script>
@stop