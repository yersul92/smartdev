@extends('layouts.default')
@section('content')
    <div class="block-4 bg-1 indent-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h2 class="title title-3">Most popular scripts</h2>
						<table class="table-1">
							<colgroup>
								<col class="col_1">
								<col class="col_2">
								<col class="col_3">
								<col class="col_4">
							</colgroup>
							<thead>
								<tr>
									<td class="first_col">Listing</td>
									<td>Total Ratings</td>
									<td>Views</td>
									<td>Added On</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="first_col">
										<h6><a href="#">Apache Friends XAMPP</a></h6>
										<p>(Scripts / PHP / Software & Servers / Installation Kits)</p>
										<span>XAMPP is a very easy to install Apache Distribution for Linux, Windows, MacOS X and Solaris.</span>
									</td>
									<td>42,928</td>
									<td>121,744</td>
									<td>09/16/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">Cute News</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / News Publishing)</p>
										<span>A really powerful news management system that uses flat files.</span>
									</td>
									<td>33,866</td>
									<td>187,148</td>
									<td>01/24/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">Coppermine Photo Gallery</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Image Galleries)</p>
										<span>Coppermine is a web picture gallery script written in PHP using GD lib or ImageMagick and MySQL.</span>
									</td>
									<td>16,668</td>
									<td>164,822</td>
									<td>10/01/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">TYPO3 CMS</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Content Management)</p>
										<span>A professional Content Management System.</span>
									</td>
									<td>13,559</td>
									<td>195,821</td>
									<td>12/18/2000</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">JS calendar</a></h6>
										<p>(Scripts / JavaScript / Scripts & Programs / Calendars)</p>
										<span>Cool, CSS-customizable date/time picker.</span>
									</td>
									<td>13,412</td>
									<td>88,870</td>
									<td>09/18/2002</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">phpBB</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Discussion Boards)</p>
										<span>A discussion board program, built in PHP backended by MySQL, PostgreSQL, MSSQL and others.</span>
									</td>
									<td>11,669</td>
									<td>163,722</td>
									<td>12/11/2000</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="block-1 indent-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h4 class="title title-1">Top Rated Scripts</h4>
						<table class="table-2">
							<colgroup>
								<col class="col_1">
								<col class="col_2">
								<col class="col_3">
								<col class="col_4">
							</colgroup>
							<thead>
								<tr>
									<td class="first_col">Listing</td>
									<td>Total Ratings</td>
									<td>Views</td>
									<td>Added On</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="first_col">
										<h6><a href="#">MaPa Link Directory</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Link Indexing)</p>
										<span>PHP MySQL link directory script.</span>
									</td>
									<td>9</td>
									<td>133</td>
									<td>01/04/2015</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">MaPa Scripts Directory</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Software Repository)</p>
										<span>PHP MySQL free scripts directory.</span>
									</td>
									<td>9</td>
									<td>189</td>
									<td>08/02/2014</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">JUnitConvert - Units of Measure Converter</a></h6>
										<p>(Scripts / Java / Applets / Miscellaneous)</p>
										<span>JUnitConv is an universal Units of Measure Converter built as a Java Applet.</span>
									</td>
									<td>6</td>
									<td>2,289</td>
									<td>05/28/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">iBULC - Internet Batch Up-Load Component</a></h6>
										<p>(Scripts / CGI & Perl / Scripts & Programs / File Manipulation / File Management)</p>
										<span>Integrate batch file uploading into your Perl, PHP, ASP scripts or programs.</span>
									</td>
									<td>6</td>
									<td>991</td>
									<td>09/15/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">OpenBiz - the Open Business Software Framework</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Development Tools)</p>
										<span>OpenBiz provides an Open Business Software Framework.</span>
									</td>
									<td>6</td>
									<td>6,548</td>
									<td>10/01/2003</td>
								</tr>
								<tr>
									<td class="first_col">
										<h6><a href="#">Another PHPagecounter</a></h6>
										<p>(Scripts / PHP / Scripts & Programs / Counters / Text Based)</p>
										<span>Free Sitewide Counter for PHP.</span>
									</td>
									<td>6</td>
									<td>2,329</td>
									<td>12/13/2003</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
@stop