@extends('layouts.default')

@section('content')
    <div class="block-4 bg-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h2 class="title title-3">About us</h2>
            <div class="thumb-pad-1">
              <div class="thumbnail">
                <img src="img/page2-img1.jpg" alt="photo">
                <div class="caption">
                  <h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</h6>
                  <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti
                    atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non animi, id est laborum
                    et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h2 class="title title-3">Our history</h2>
            <ul class="list list-3">
              <li>
                <div class="date"><span>20</span>August</div>
                <div class="wrapper">
                  <h6>Perspiciatis unde omnis ist.</h6>
                  <p>At vero eos et accusamus et iusto odio dignissivoquas molestias excepturi sint occaecati cupiditate non
                    provident, similique sunt in culpa qui officia animi, id est laborum et dolorum fuga. Et harum quidem
                    rerum facilis est.</p>
                </div>
              </li>
              <li>
                <div class="date"><span>26</span>August</div>
                <div class="wrapper">
                  <h6>Nulla imperdiet mauris vel.</h6>
                  <p>Pellentesque finibus quis mauris quis congue. Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar.
                    Etiam in arcu velit. Aenean risus accumsan purus et purus ultricies feugiat. Nulla mauris vel iaculis
                    convallis. </p>
                </div>
              </li>
              <li>
                <div class="date"><span>28</span>August</div>
                <div class="wrapper">
                  <h6>Mauris vel iaculis convallis. </h6>
                  <p>Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar. Etiam in arcu velit. Aenean risus feugiat.
                    Nulla imperdiet mauris vel iaculis convallis. Donec mattis, purus eu malesuada malesuada, mi sapien porta
                    diam.</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h2 class="title title-3">What we do</h2>
            <div class="box box-3">
              <h6>Perspiciatis unde omnis ist.</h6>
              <p>At vero eos et accusamus et iusto odio dignissivoquas molestias excepturi sint occaecati cupiditate non provident,
                similique sunt in culpa qui officia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis
                est.</p>
              <ul class="list list-2">
                <li>Lorem ipsum dolor sit </li>
                <li>Amet, consectetur </li>
                <li>Adipiscing elit fusce dapibus </li>
                <li>Nisi ex vel dictum dui pretium </li>
                <li>Praesent lacinia </li>
                <li>Dui eget risus ultricies</li>
                <li>Id venenatis enim ultrices</li>
                <li>Curabitur sit amet </li>
                <li>Vivamus eros augue</li>
                <li>Luctus pharetra mollis fringilla</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-1 indent-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="title title-1">Our technicians</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="thumb-pad-2">
                  <div class="thumbnail">
                    <img src="img/page2-img2.jpg" alt="photo">
                    <div class="caption">
                      <h6>Mark Johnson</h6>
                      <p>Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar. Etiam in arcu velit. Aenean risus.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="thumb-pad-2">
                  <div class="thumbnail">
                    <img src="img/page2-img3.jpg" alt="photo">
                    <div class="caption">
                      <h6>Bradley Grosh</h6>
                      <p>Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar. Etiam in arcu velit. Aenean risus.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="thumb-pad-2">
                  <div class="thumbnail">
                    <img src="img/page2-img4.jpg" alt="photo">
                    <div class="caption">
                      <h6>Sam Kromstain</h6>
                      <p>Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar. Etiam in arcu velit. Aenean risus.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="thumb-pad-2">
                  <div class="thumbnail">
                    <img src="img/page2-img5.jpg" alt="photo">
                    <div class="caption">
                      <h6>Caroline Beek </h6>
                      <p>Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar. Etiam in arcu velit. Aenean risus.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="block-2 bg-img-3 indent-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h3 class="title title-2">Mission & values</h3>
            <ul class="list list-4">
              <li>
                <div class="number">1.</div>
                <div class="wrapper">
                  <h6>Maiores alias est consequ.</h6>
                  <p>Pellentesque finibus quis mauris quis congue. Proin quis volutpat odio. Mauris pellentesque at leo at pulvinar.
                    Etiam in arcu velit. Aenean risus accumsan purus.</p>
                </div>
              </li>
              <li>
                <div class="number">2.</div>
                <div class="wrapper">
                  <h6>Aenean risus augue, suscipit.</h6>
                  <p>Et purus ultricies feugiat. Nulla mauris vel iaculis malesuada, mi sapien porta diam, eget tincidunt turpis
                    felis a enim. Ut lobortis elementum leo eget bibendum felis lacus.</p>
                </div>
              </li>
              <li>
                <div class="number">3.</div>
                <div class="wrapper">
                  <h6>Cras luctus rutrum scelerisque.</h6>
                  <p>Donec mattis, purus eu malesuada malesuada, mi sapien porta diam, eget tincidunt turpis felis a enim. Ut
                    lobortis elementum leo eget condimentum. Nullam bibendum feli.</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h3 class="title title-2">Related info </h3>
            <div class="box box-3">
              <h6>Donec vulputate, quam fringilla.</h6>
              <p>Accusamus et iusto odio dignissivoquas molestias excepturi sint occaecati cupiditate non provident, similique
                sunt in culpa qui officia animi, id est laborum et dolorum fuga. </p>
              <ul class="list list-2">
                <li>Adipiscing elit fusce dapibus </li>
                <li>Nisi ex vel dictum dui pretium </li>
                <li>Praesent lacinia </li>
                <li>Dui eget risus ultricies</li>
                <li>Id venenatis enim ultrices</li>
                <li>Curabitur sit amet</li>
                <li>Vivamus eros augue</li>
                <li>Luctus pharetra mollis </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h3 class="title title-2">cliens say </h3>
            <div class="box box-4">
              <blockquote>
                <p>Sed urna eros, ultrices a felis in, faucibus interdum sapien. Sed molestie magna nec elit sagittis pharetra.
                  Aliquam tincidunt metus in euismod tincidunt. Vivamus at dolor justo. Fusce a tellus at quam euismod tempor
                  id quis mi. placerat nec. Praesent cursus et ligula sit amet sodales. posuere nibh nec, fringilla orci.
                  Cras luctus scelerisque. Vestibulum ullamcorper.</p>
              </blockquote>
              <h6>Thomas Bishop, client</h6>
            </div>
            <div class="box box-4">
              <blockquote>
                <p>Mollis augue id lacinia. Curabitur sit amet venenatis erat. Donec ullamcorper ornare condimentum. Nam in
                  blandit ligula. Maecenas dictum congue vulputate. Pellentesque habitant morbi tristique senectus et netus
                  et malesuada fames ac turpis egestas. Proin pretium tincidunt euismod. faucibus. Ut porttitor aliquet nulla
                  in interdum. Vivamus nec elit ut ex mattis tincidunt. </p>
              </blockquote>
              <h6>Steven Donovan, client</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop