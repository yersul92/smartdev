@extends('layouts.default')
@section('content')
    <div class="camera_block">
        <div class="camera_wrap">
            <div data-src="img/slide-1.jpg">
                <div class="camera_caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2>Comprehensive software solution</h2>
                                <h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <br>doloremque laudantium,totam rem aperiam.</h6>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia.</p>
                                <a href="#" class="btn-default btn btn-1">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div data-src="img/slide-2.jpg">
                <div class="camera_caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2>Quality PHP scripts / PHP Software</h2>
                                <h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <br>doloremque laudantium,totam rem aperiam.</h6>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia.</p>
                                <a href="#" class="btn-default btn btn-1">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-src="img/slide-3.jpg">
                <div class="camera_caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2>We provide an extremely low price</h2>
                                <h6>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium <br>doloremque laudantium,totam rem aperiam.</h6>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia.</p>
                                <a href="#" class="btn-default btn btn-1">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    
    <div class="block-1 bg-img-2">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<h4 class="title title-1">Bestseller scripts</h4>
								<ul class="list list-2">
									<li><a href="#">Super B2B Marketplace <span>($149)</span></a>
									</li>
									<li><a href="#">#1 Job board Software <span>($149)</span></a>
									</li>
									<li><a href="#">Best Auction Software <span>($149)</span></a>
									</li>
									<li><a href="#">Top Classified Software <span>($149)</span></a>
									</li>
									<li><a href="#">Business Directory Script <span>($149)</span></a>
									</li>
									<li><a href="#">Freelance Script <span>($149)</span></a>
									</li>
									<li><a href="#">PHP Shopping Cart Script <span>($149)</span></a>
									</li>
									<li><a href="#">Car Classifieds Script <span>($149)</span></a>
									</li>
									<li><a href="#">Penny Auction Script <span>($149)</span></a>
									</li>
									<li><a href="#">Ad Management Script <span>($55)</span></a>
									</li>
									<li><a href="#">Banner Exchange Script <span>($45)</span></a>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<h4 class="title title-1">New arrivals</h4>
								<ul class="list list-2">
									<li><a href="#">Business Directory Script <span>($149)</span></a>
									</li>
									<li><a href="#">Lowest Unique Bid Script <span>($149)</span></a>
									</li>
									<li><a href="#">Shopping Cart Software <span>($149)</span></a>
									</li>
									<li><a href="#">Super Gallery Script <span>($49)</span></a>
									</li>
									<li><a href="#">Nice News Script <span>($39)</span></a>
									</li>
									<li><a href="#">Softbiz Hit Counter <span>($49)</span></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<h4 class="title title-1">Economy Scripts</h4>
								<ul class="list list-2">
									<li><a href="#">Softbiz Dating Script <span>($99)</span></a>
									</li>
									<li><a href="#">Job Board Script <span>($79)</span></a>
									</li>
									<li><a href="#">Classified Ads Script <span>($79)</span></a>
									</li>
									<li><a href="#">Shopping Cart Script <span>($79)</span></a>
									</li>
									<li><a href="#">Auction Script PLUS <span>($79)</span></a>
									</li>
									<li><a href="#">Auctions Script <span>($49)</span></a>
									</li>
									<li><a href="#">Classifieds Script <span>($49)</span></a>
									</li>
									<li><a href="#">Link Directory Script <span>($39)</span></a>
									</li>
									<li><a href="#">FAQ Script <span>($19)</span></a>
									</li>
									<li><a href="#">Image Gallery Script <span>($29)</span></a>
									</li>
									<li><a href="#">Automatic Link Checkerk <span>($10)</span></a>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<h4 class="title title-1">more scripts</h4>
								<ul class="list list-2">
									<li><a href="#">Job Board Software <span>($99)</span></a>
									</li>
									<li><a href="#">Online Auction Software <span>($99)</span></a>
									</li>
									<li><a href="#">B2B Marketplace Script <span>($99)</span></a>
									</li>
									<li><a href="#">Classifieds Software <span>($99)</span></a>
									</li>
									<li><a href="#">Auto Classifieds Script <span>($99)</span></a>
									</li>
									<li><a href="#">PHP Shopping Cart Software <span>($99)</span></a>
									</li>
									<li><a href="#">Online Dating Software <span>($125)</span></a>
									</li>
									<li><a href="#">PHP Directory Software <span>($49)</span></a>
									</li>
									<li><a href="#">Banner Ad Management Script <span>($21)</span></a>
									</li>
									<li><a href="#">Link Exchange Scriptt <span>($19)</span></a>
									</li>
									<li><a href="#">Web Hosting Directory Script <span>($75)</span></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    <div class="block-2 bg-img-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3 class="title title-2">Featured Listings</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="box box-1 indent-1">
								<div class="wrapper">
									<div class="number">01.</div>
									blanditiis praesentium voluptatum
								</div>
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atquecorrupti
									quos.</p>
								<a href="#" class="btn-default btn btn-2">More</a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="box box-1 indent-1">
								<div class="wrapper">
									<div class="number">02.</div>
									quis nostrud exercitation
								</div>
								<p>Wro eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
									quosdolor.</p>
								<a href="#" class="btn-default btn btn-2">More</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="box box-1 indent-1">
								<div class="wrapper">
									<div class="number">03.</div>
									cumque nihil impedit quo minus
								</div>
								<p>ES vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti.
									</p>
								<a href="#" class="btn-default btn btn-2">More</a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="box box-1 indent-1">
								<div class="wrapper">
									<div class="number">04.</div>
									maiores alias est consequatur
								</div>
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti.
									</p>
								<a href="#" class="btn-default btn btn-2">More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block-3 bg-img-4">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
					<div class="box box-2">
						<h2>Trusted by more</h2>
						<strong>than <span>300 000</span></strong>
						<h3>customers</h3>
						<a href="#" class="btn-default btn btn-3">learn more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	 <script>
		jQuery(document).ready(function () {
			jQuery('.camera_wrap').camera({
				pagination: true,
				navigation: false,
				navigationHover: false,
				loaderOpacity: '0',
				fx: 'simpleFade',
				height: '28.5%',
				minHeight: '500px'
			});
			jQuery(".box-1 .btn").on("mouseenter", function() {
				jQuery(this).parents(".box-1").find(".wrapper").addClass("hovered");
			});
			jQuery(".box-1 .btn").on("mouseout", function() {
				jQuery(this).parents(".box-1").find(".wrapper").removeClass("hovered");
			});
		});
	</script>
@stop
