@extends('layouts.default')
@section('content')
    		<div class="block-4 bg-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h2 class="title title-3">Contact info</h2>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<dl class="list list-7">
									<dt>8901 Marmora Road, <br>Glasgow, D04 89GR.</dt>
									<dd><span>Freephone:</span>+1 800 559 6580</dd>
									<dd><span>Telephone:</span>+1 800 603 6035</dd>
									<dd><span>FAX:</span>+1 800 889 9898 </dd>
									<dd>E-mail: <a href="#">mail@demolink.org</a>
									</dd>
								</dl>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<dl class="list list-7">
									<dt>9863 - 9867 Mill Road, <br>Cambridge, MG09 99HT.</dt>
									<dd><span>Freephone:</span>+1 800 559 6580</dd>
									<dd><span>Telephone:</span>+1 800 603 6035</dd>
									<dd><span>FAX:</span>+1 800 889 9898 </dd>
									<dd>E-mail: <a href="#">mail@demolink.org</a>
									</dd>
								</dl>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
								<dl class="list list-7">
									<dt>9870 St Vincent Place, <br>Glasgow, DC 45 Fr 45.</dt>
									<dd><span>Freephone:</span>+1 800 559 6580</dd>
									<dd><span>Telephone:</span>+1 800 603 6035</dd>
									<dd><span>FAX:</span>+1 800 889 9898 </dd>
									<dd>E-mail: <a href="#">mail@demolink.org</a>
									</dd>
								</dl>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="google-map-api">
			<div id="map-canvas" class="gmap"></div>
		</div>
		<div class="block-2 bg-img-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h3 class="title title-2">Contact form</h3>
						<form id="contact-form" method="POST" action="bat/MailHandler.php">
							<div class="contact-form-loader"></div>
							<fieldset>
								<label class="name form-div-1">
<input type="text" name="name" value="" data-constraints="@Required @JustLetters" id="regula-generated-421247" class="">
<span class="empty-message">*This field is required.</span>
<span class="error-message">*This is not a valid name.</span>
<span class="_placeholder" style="left: 0px; top: 0px; width: 332px; height: 38px;">Name: </span></label>
								<label class="phone form-div-3">
<input type="text" name="phone" value="" data-constraints="@Required @JustNumbers" id="regula-generated-413557" class="">
<span class="empty-message">*This field is required.</span>
<span class="error-message">*This is not a valid phone.</span>
<span class="_placeholder" style="left: 0px; top: 0px; width: 332px; height: 38px;">Phone:</span></label>
								<label class="email form-div-2">
<input type="text" name="email" value="" data-constraints="@Required @Email" id="regula-generated-707796" class="">
<span class="empty-message">*This field is required.</span>
<span class="error-message">*This is not a valid email.</span>
<span class="_placeholder" style="left: 0px; top: 0px; width: 332px; height: 38px;">Email:</span></label>
								<label class="message form-div-4">
<textarea name="message" data-constraints="@Length(min=20,max=999999)" id="regula-generated-803661" class="">
									</textarea>
<span class="empty-message">*This field is required.</span>
<span class="error-message">*The message is too short.</span>
<span class="_placeholder" style="left: 0px; top: 0px; width: 1132px; height: 287px;">Message:</span></label>

								<div class="btns">
									<a href="#" data-type="submit" class="btn-default btn btn-2">send</a>
								</div>
							</fieldset>
							<div class="modal fade response-message">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
×
</button>
											<h4 class="modal-title">Modal title</h4>
										</div>
										<div class="modal-body">
											You message has been sent! We will be in touch soon.
										</div>
									</div>
								</div>
							</div>
						<input type="hidden" name="stripHTML" value="true"></form>
					</div>
				</div>
			</div>
		</div>
											<h4 class="modal-title">Modal title</h4>
										</div>
										<div class="modal-body">
											You message has been sent! We will be in touch soon.
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script>
		google_api_map_init();

		function google_api_map_init() {
			var map;
			var coordData = new google.maps.LatLng(parseFloat(40.646197), parseFloat(-73.9724068, 14));
			var markCoord1 = new google.maps.LatLng(parseFloat(40.643180), parseFloat(-73.9874068, 14));
			var markCoord2 = new google.maps.LatLng(parseFloat(40.6422180), parseFloat(-73.9784068, 14));
			var markCoord3 = new google.maps.LatLng(parseFloat(40.6482180), parseFloat(-73.9724068, 14));
			var markCoord4 = new google.maps.LatLng(parseFloat(40.6442180), parseFloat(-73.9664068, 14));
			var markCoord5 = new google.maps.LatLng(parseFloat(40.6379180), parseFloat(-73.9552068, 14));
			var marker;

			var styleArray = [
				{
					"featureType": "administrative",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#444444"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"color": "#f2f2f2"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 45
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
						{
							"color": "#46bcec"
						},
						{
							"visibility": "on"
						}
					]
				}
			];
			var markerIcon = {
				url: "img/gmap_marker.png",
				size: new google.maps.Size(42, 124),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(21, 70)
			};
			/*var markerIcon = { 
					url: "img/gmap_marker.png", 
					size: new google.maps.Size(53, 71), 
					origin: new google.maps.Point(0,0), 
					anchor: new google.maps.Point(21, 70) 
			}; */
			function initialize() {
				var mapOptions = {
					zoom: 14,
					center: coordData,
					scrollwheel: false,
					styles: styleArray
				}

				var contentString = "<div></div>";
				var infowindow = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 200
				});
				marker = new google.maps.Marker({
					map: map,
					draggable: true,
					position: coordData,
					icon: markerIcon
				});
				var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
				marker = new google.maps.Marker({
					map: map,
					position: markCoord1,
					icon: markerIcon
				});

				/*marker1 = new google.maps.Marker({ 
						map:map, 
						position: markCoord2, 
						icon: markerIcon
				}); 
	
				marker2 = new google.maps.Marker({ 
						map:map, 
						position: markCoord3, 
						icon: markerIcon
				}); 
	
				marker3 = new google.maps.Marker({ 
						map:map, 
						position: markCoord4, 
						icon: markerIcon
				}); 
	
				marker4 = new google.maps.Marker({ 
						map:map, 
						position: markCoord5, 
						icon: markerIcon
				}); */

				var contentString = '<div id="content">' +
					'<div id="siteNotice">' +
					'</div>' +
					'<div id="bodyContent">' +
					'<p>4578 Marmora Road, Glasgow D04 89GR <span>800 2345-6789</span></p>' +
					'</div>' +
					'</div>';

				var contentString1 = '<div id="content">' +
					'<div id="siteNotice">' +
					'</div>' +
					'<div id="bodyContent">' +
					'<p>4578 Marmora Road, Glasgow D04 89GR <span>800 2345-6789</span></p>' +
					'</div>' +
					'</div>';
				/*
				var contentString2 = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<div id="bodyContent">'+
				'<p>4578 Marmora Road, Glasgow D04 89GR <span>800 2345-6789</span></p>'+
				'</div>'+
				'</div>';
	
				var contentString3 = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<div id="bodyContent">'+
				'<p>4578 Marmora Road, Glasgow D04 89GR <span>800 2345-6789</span></p>'+
				'</div>'+
				'</div>';
	
				var contentString4 = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<div id="bodyContent">'+
				'<p>4578 Marmora Road, Glasgow D04 89GR <span>800 2345-6789</span></p>'+
				'</div>'+
				'</div>';*/

				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				/*var infowindow1 = new google.maps.InfoWindow({
						content: contentString1
				});
	
				var infowindow2 = new google.maps.InfoWindow({
						content: contentString2
				});
	
				var infowindow2 = new google.maps.InfoWindow({
						content: contentString3
				});
	
				var infowindow2 = new google.maps.InfoWindow({
						content: contentString4
				});*/



				google.maps.event.addListener(marker, 'click', function () {
					infowindow.open(map, marker);
					$('.gm-style-iw').parent().parent().addClass("gm-wrapper");
				});

				/*google.maps.event.addListener(marker1, 'click', function() {
					infowindow.open(map,marker1);
					$('.gm-style-iw').parent().parent().addClass("gm-wrapper");
				});
	
				google.maps.event.addListener(marker2, 'click', function() {
					infowindow.open(map,marker2);
					$('.gm-style-iw').parent().parent().addClass("gm-wrapper");
				});
	
				google.maps.event.addListener(marker3, 'click', function() {
					infowindow.open(map,marker3);
					$('.gm-style-iw').parent().parent().addClass("gm-wrapper");
				});
	
				google.maps.event.addListener(marker4, 'click', function() {
					infowindow.open(map,marker4);
					$('.gm-style-iw').parent().parent().addClass("gm-wrapper");
				});*/

				google.maps.event.addDomListener(window, 'resize', function () {

					map.setCenter(coordData);

					var center = map.getCenter();
				});
			}

			google.maps.event.addDomListener(window, "load", initialize);

		}
	</script>
	<script>
		if (jQuery("body").width() < 991) {
			jQuery('.list-7').equalHeights();
		}
		jQuery(window).resize(function () {
			jQuery('.list-7').height('auto');
			if (jQuery("body").width() < 991) {
				jQuery('.list-7').equalHeights();
			}
		});
	</script>
@stop