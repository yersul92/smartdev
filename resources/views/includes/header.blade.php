<div class="header_top">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="left_side">
                    <a href="./" class="brand">
                        <h1 class="brand_name">smart<span>DEV</span></h1>
                    </a>
                </div>
                <div class="right_side">
                    <ul class="list list-1">
                        <li class="create-account"><a href="#">@lang('dict.sign_up')</a>
                        </li>
                        <li class="login"><a href="#">@lang('dict.sign_in')</a>
                        </li>
                        <li>
                            <ul class="list social-list">
                                <li><a href="#"><i class="fa fa-tumblr-square"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header_bottom bg-img-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="left_side">
                    <nav class="nav navbar-default navbar-static-top tm_navbar clearfix" role="navigation">
                        <ul class="sf-menu">
                            <li {{{ (Request::is('home') ? 'class=active' : '') }}}>
                                <a href="./">@lang('dict.home')</a>
                                <!--ul>
                                    <li>
                                        <a href="#">history</a>
                                    </li>
                                    <li class="with_ul">
                                        <a href="#">news</a>
                                        <ul>
                                            <li>
                                                <a href="#">latest</a>
                                            </li>
                                            <li>
                                                <a href="#">archive</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">testimonials</a>
                                    </li>
                                </ul-->
                            </li>
                            <li {{{ (Request::is('about') ? 'class=active' : '') }}}>
                                <a href="./about">@lang('dict.about_us')</a>
                            </li>
                            <li {{{ (Request::is('services') ? 'class=active' : '') }}}>
                                <a href="./services">@lang('dict.services')</a>
                            </li>
                             <li {{{ (Request::is('faqs') ? 'class=active' : '') }}}>
                                <a href="./faqs">@lang('dict.faqs')</a>
                            </li>
                            <li {{{ (Request::is('contacts') ? 'class=active' : '') }}}>
                                <a href="./contacts">@lang('dict.contacts')</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="right_side">
                    <form id="search" class="search wow fadeInUp" action="search.php" method="GET" accept-charset="utf-8" data-wow-delay="0.4s">
                                <input type="text" name="s" value="" onfocus="if (this.value == '') {this.value=''}" onblur="if (this.value == '') {this.value=''}">
                                <a href="#" onClick="document.getElementById('search').submit()" class="btn-default my-btn">
                                    <i class="fa fa-search"></i>
                                </a>
                            </form>
                </div>
            </div>
        </div>
    </div>
</div>