<div class="footer_top parallax-bg-1 parallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="box box-3">
                            <i class="fa  fa-map-marker"></i>
                            <p>New York 9870 St Vincent </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="box box-3">
                            <i class="fa fa-envelope"></i>
                            <p>contact@demolink.org</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="box box-3">
                            <i class="fa fa-phone"></i>
                            <p>Telephone: +1 800 603 6035</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="box box-3">
                            <i class="fa fa-fax"></i>
                            <p>FAX: +1 800 889 9898</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_middle">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="left_side">
                    <h4>Have Questions? <br>Ask a specialist</h4>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="middle_side">
                    <h2>1 800 559 6580</h2>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="right_side">
                    <h4>7 days a week <br>from 8:00 am <br>to 3:00am</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="list social-list">
                    <li><a href="#"><i class="fa fa-tumblr-square"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-rss"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a>
                    </li>
                </ul>
                <div class="footer_privacy"> &copy; <span id="copyright-year"></span><a href="/contacts" class="link email"> Privacy policy</a>

                </div>
            </div>
        </div>
    </div>
    <a class="totop_btn" href="#"><img src="img/totop.png" alt="totop">
    </a>
</div>