<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('/', function () {
    return view('welcome');
});
*/


Route::get('/', function()
{
    return View::make('pages.home',["activeMenu"=>'home']);
});
Route::get('about', function()
{
    return View::make('pages.about')->withActiveMenu('about');
});
Route::get('services', function()
{
    return View::make('pages.most_popular')->withActiveMenu('services');
});
Route::get('contacts', function()
{
    return View::make('pages.contacts')->withActiveMenu('contacts');
});
Route::get('faqs', function()
{
    return View::make('pages.faqs')->withActiveMenu('faqs');
});